import { ViteSSG } from 'vite-ssg'

import 'virtual:windi-base.css'
import 'virtual:windi-components.css'

import 'virtual:windi-utilities.css'
import './styles/_font.scss'
import './styles.scss'

import { GesturePlugin } from '@vueuse/gesture'

import App from './App.vue'

export const createApp = ViteSSG(App, { routes: [] }, app => {
  app.app.use(GesturePlugin)
})
